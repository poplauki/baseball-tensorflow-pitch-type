<!--
--- Readme.md Snippet without images Start ---
## Tech Stack
wirtaw/baseball-tensorflow-pitch-type is built on the following main stack:
- [Swift](https://developer.apple.com/swift/) – Languages
- [Socket.IO](http://socket.io/) – Realtime Backend / API
- [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) – Languages
- [Babel](http://babeljs.io/) – JavaScript Compilers
- [ESLint](http://eslint.org/) – Code Review
- [Shell](https://en.wikipedia.org/wiki/Shell_script) – Shells
- [TensorFlow](https://www.tensorflow.org) – Machine Learning Tools
- [Parcel](https://parceljs.org/) – JS Build Tools / JS Task Runners
- [Docker](https://www.docker.com/) – Virtual Machine Platforms & Containers

Full tech stack [here](/techstack.md)
--- Readme.md Snippet without images End ---

--- Readme.md Snippet with images Start ---
## Tech Stack
wirtaw/baseball-tensorflow-pitch-type is built on the following main stack:
- <img width='25' height='25' src='https://img.stackshare.io/service/1009/tuHsaI2U.png' alt='Swift'/> [Swift](https://developer.apple.com/swift/) – Languages
- <img width='25' height='25' src='https://img.stackshare.io/service/1161/vI0ZZlhZ_400x400.png' alt='Socket.IO'/> [Socket.IO](http://socket.io/) – Realtime Backend / API
- <img width='25' height='25' src='https://img.stackshare.io/service/1209/javascript.jpeg' alt='JavaScript'/> [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) – Languages
- <img width='25' height='25' src='https://img.stackshare.io/service/2739/-1wfGjNw.png' alt='Babel'/> [Babel](http://babeljs.io/) – JavaScript Compilers
- <img width='25' height='25' src='https://img.stackshare.io/service/3337/Q4L7Jncy.jpg' alt='ESLint'/> [ESLint](http://eslint.org/) – Code Review
- <img width='25' height='25' src='https://img.stackshare.io/service/4631/default_c2062d40130562bdc836c13dbca02d318205a962.png' alt='Shell'/> [Shell](https://en.wikipedia.org/wiki/Shell_script) – Shells
- <img width='25' height='25' src='https://img.stackshare.io/service/4717/FtFnqC38_400x400.png' alt='TensorFlow'/> [TensorFlow](https://www.tensorflow.org) – Machine Learning Tools
- <img width='25' height='25' src='https://img.stackshare.io/service/8054/fC6Wad-S_400x400.jpg' alt='Parcel'/> [Parcel](https://parceljs.org/) – JS Build Tools / JS Task Runners
- <img width='25' height='25' src='https://img.stackshare.io/service/586/n4u37v9t_400x400.png' alt='Docker'/> [Docker](https://www.docker.com/) – Virtual Machine Platforms & Containers

Full tech stack [here](/techstack.md)
--- Readme.md Snippet with images End ---
-->
<div align="center">

# Tech Stack File
![](https://img.stackshare.io/repo.svg "repo") [wirtaw/baseball-tensorflow-pitch-type](https://github.com/wirtaw/baseball-tensorflow-pitch-type)![](https://img.stackshare.io/public_badge.svg "public")
<br/><br/>
|21<br/>Tools used|11/11/23 <br/>Report generated|
|------|------|
</div>

## <img src='https://img.stackshare.io/languages.svg'/> Languages (2)
<table><tr>
  <td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1209/javascript.jpeg' alt='JavaScript'>
  <br>
  <sub><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript">JavaScript</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1009/tuHsaI2U.png' alt='Swift'>
  <br>
  <sub><a href="https://developer.apple.com/swift/">Swift</a></sub>
  <br>
  <sub></sub>
</td>

</tr>
</table>

## <img src='https://img.stackshare.io/databases.svg'/> Data (1)
<table><tr>
  <td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1161/vI0ZZlhZ_400x400.png' alt='Socket.IO'>
  <br>
  <sub><a href="http://socket.io/">Socket.IO</a></sub>
  <br>
  <sub>v4.7.2</sub>
</td>

</tr>
</table>

## <img src='https://img.stackshare.io/devops.svg'/> DevOps (7)
<table><tr>
  <td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/2739/-1wfGjNw.png' alt='Babel'>
  <br>
  <sub><a href="http://babeljs.io/">Babel</a></sub>
  <br>
  <sub>v7.22.5</sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/586/n4u37v9t_400x400.png' alt='Docker'>
  <br>
  <sub><a href="https://www.docker.com/">Docker</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/8067/default_90dcb1286af7685c68df319c764b80704df1155b.png' alt='Dotenv'>
  <br>
  <sub><a href="https://github.com/motdotla/dotenv">Dotenv</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/3337/Q4L7Jncy.jpg' alt='ESLint'>
  <br>
  <sub><a href="http://eslint.org/">ESLint</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1046/git.png' alt='Git'>
  <br>
  <sub><a href="http://git-scm.com/">Git</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/8054/fC6Wad-S_400x400.jpg' alt='Parcel'>
  <br>
  <sub><a href="https://parceljs.org/">Parcel</a></sub>
  <br>
  <sub>v1.12.5</sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/1120/lejvzrnlpb308aftn31u.png' alt='npm'>
  <br>
  <sub><a href="https://www.npmjs.com/">npm</a></sub>
  <br>
  <sub></sub>
</td>

</tr>
</table>

## Other (3)
<table><tr>
  <td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/2426/e1cbdef9d4b11484049a033886578e54_400x400.png' alt='CocoaPods'>
  <br>
  <sub><a href="https://cocoapods.org/">CocoaPods</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/4631/default_c2062d40130562bdc836c13dbca02d318205a962.png' alt='Shell'>
  <br>
  <sub><a href="https://en.wikipedia.org/wiki/Shell_script">Shell</a></sub>
  <br>
  <sub></sub>
</td>

<td align='center'>
  <img width='36' height='36' src='https://img.stackshare.io/service/4717/FtFnqC38_400x400.png' alt='TensorFlow'>
  <br>
  <sub><a href="https://www.tensorflow.org">TensorFlow</a></sub>
  <br>
  <sub></sub>
</td>

</tr>
</table>


## <img src='https://img.stackshare.io/group.svg' /> Open source packages (8)</h2>

## <img width='24' height='24' src='https://img.stackshare.io/service/1120/lejvzrnlpb308aftn31u.png'/> npm (8)

|NAME|VERSION|LAST UPDATED|LAST UPDATED BY|LICENSE|VULNERABILITIES|
|:------|:------|:------|:------|:------|:------|
|[adm-zip](https://www.npmjs.com/adm-zip)|v0.5.10|01/29/23|Poplavskij Vladimir |MIT|N/A|
|[argparse](https://www.npmjs.com/argparse)|v1.0.10|11/12/20|Poplavskij Vladimir |MIT|N/A|
|[babel-eslint](https://www.npmjs.com/babel-eslint)|v10.1.0|11/13/21|Poplavskij Vladimir |MIT|N/A|
|[eslint-config-loopback](https://www.npmjs.com/eslint-config-loopback)|v13.1.0|11/13/21|Poplavskij Vladimir |MIT|N/A|
|[mkdirp](https://www.npmjs.com/mkdirp)|v0.5.6|02/28/21|Poplavskij Vladimir |MIT|N/A|
|[parcel-bundler](https://www.npmjs.com/parcel-bundler)|v1.12.5|07/22/22|Poplavskij Vladimir |MIT|N/A|
|[serialize-javascript](https://www.npmjs.com/serialize-javascript)|v6.0.1|01/29/23|Poplavskij Vladimir |BSD-3-Clause|N/A|
|[socket.io-client](https://www.npmjs.com/socket.io-client)|v4.7.2|10/08/23|Vladimir Poplavskij |MIT|N/A|

<br/>
<div align='center'>

Generated via [Stack File](https://github.com/apps/stack-file)
